package com.solartrackr.egauge.widget.util;

public interface Callback {

    void callback(Object object);

}
